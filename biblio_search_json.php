<?php
/* This file is part of a copyrighted work; it is distributed with NO WARRANTY.
 * See the file COPYRIGHT.html for more details.
 */
setlocale(LC_ALL, 'en_US.utf8'); 
 
  require_once("../shared/common.php");
  session_cache_limiter(null);

  #****************************************************************************
  #*  Checking for post vars.  Go back to form if none found.
  #****************************************************************************
  if (count($_POST) == 0) {
    header("Location: ../catalog/index.php");
    exit();
  }

  #****************************************************************************
  #*  Checking for tab name to show OPAC look and feel if searching from OPAC
  #****************************************************************************
  $tab = "opac";
  $nav = "home";
  $helpPage = "opac";
  $focus_form_name = "phrasesearch";
  $focus_form_field = "searchText";
  require_once("../classes/Localize.php");

//  if (isset($_POST["lookup"])) {
//    $lookup = $_POST["lookup"];
//    if ($lookup == 'Y') {
//      $helpPage = "opacLookup";
//    }
//  }

  $nav = "search";
  if ($tab != "opac") {
    require_once("../shared/logincheck.php");
  }
  require_once("../classes/BiblioSearch.php");
  require_once("../classes/BiblioSearchQuery.php");
  require_once("../functions/searchFuncs.php");
  require_once("../classes/DmQuery.php");

  #****************************************************************************
  #*  Function declaration only used on this page.
  #****************************************************************************
  function printResultPages(&$loc, $currPage, $pageCount, $sort) {
header('Content-Type: application/json');
    if ($pageCount <= 1) {
      return false;
    }
    echo $loc->getText("biblioSearchResultPages").": ";
    $maxPg = OBIB_SEARCH_MAXPAGES + 1;
    if ($currPage > 1) {
      echo "<a href=\"javascript:changePage(".H(addslashes($currPage-1)).",'".H(addslashes($sort))."')\">&laquo;".$loc->getText("biblioSearchPrev")."</a> ";
    }
    for ($i = 1; $i <= $pageCount; $i++) {
      if ($i < $maxPg) {
        if ($i == $currPage) {
          echo "<b>".H($i)."</b> ";
        } else {
          echo "<a href=\"javascript:changePage(".H(addslashes($i)).",'".H(addslashes($sort))."')\">".H($i)."</a> ";
        }
      } elseif ($i == $maxPg) {
        echo "... ";
      }
    }
    if ($currPage < $pageCount) {
      echo "<a href=\"javascript:changePage(".($currPage+1).",'".$sort."')\">".$loc->getText("biblioSearchNext")."&raquo;</a> ";
    }
  }

  #****************************************************************************
  #*  Loading a few domain tables into associative arrays
  #****************************************************************************
  $dmQ = new DmQuery();
  $dmQ->connect();
  $collectionDm = $dmQ->getAssoc("collection_dm");
  $materialTypeDm = $dmQ->getAssoc("material_type_dm");
  $materialImageFiles = $dmQ->getAssoc("material_type_dm", "image_file");
  $biblioStatusDm = $dmQ->getAssoc("biblio_status_dm");
  $dmQ->close();

  #****************************************************************************
  #*  Retrieving post vars and scrubbing the data
  #****************************************************************************
  if (isset($_POST["page"])) {
    $currentPageNmbr = $_POST["page"];
  } else {
    $currentPageNmbr = 1;
  }
  $searchType = $_POST["searchType"];
  $sortBy = $_POST["sortBy"];
  if ($sortBy == "default") {
    if ($searchType == "author") {
      $sortBy = "author";
    } else {
      $sortBy = "title";
    }
  }
  $searchText = trim($_POST["searchText"]);
  # remove redundant whitespace
  $searchText = preg_replace('/\s+/', " ", $searchText);
  if ($searchType == "barcodeNmbr") {
    $sType = OBIB_SEARCH_BARCODE;
    $words[] = $searchText;
  } else {
    $words = explodeQuoted($searchText);
    if ($searchType == "author") {
      $sType = OBIB_SEARCH_AUTHOR;
    } elseif ($searchType == "subject") {
      $sType = OBIB_SEARCH_SUBJECT;
    } elseif ($searchType == "callno") {
      $sType = OBIB_SEARCH_CALLNO;
    } elseif ($searchType == "keyword") {
      $sType = OBIB_SEARCH_KEYWORD;
    } else {
      $sType = OBIB_SEARCH_TITLE;
    }
  }

  #****************************************************************************
  #*  Search database
  #****************************************************************************
  $biblioQ = new BiblioSearchQuery();
  //$biblioQ->setItemsPerPage(OBIB_ITEMS_PER_PAGE);
  $biblioQ->setItemsPerPage(50);
  $biblioQ->connect();
  if ($biblioQ->errorOccurred()) {
    $biblioQ->close();
    displayErrorPage($biblioQ);
  }
  # checking to see if we are in the opac search or logged in
  if ($tab == "opac") {
    $opacFlg = true;
  } else {
    $opacFlg = false;
  }
  if (!$biblioQ->search($sType,$words,$currentPageNmbr,$sortBy,$opacFlg)) {
    $biblioQ->close();
    displayErrorPage($biblioQ);
  }

  # Redirect to biblio_view if only one result
  //if ($biblioQ->getRowCount() == 1 && $lookup !== 'Y') {
  //  $biblio = $biblioQ->fetchRow();
  //  header('Location: ../shared/biblio_view.php?bibid='.U($biblio->getBibid()).'&tab='.U($tab));
  //  exit();
 // }
  
  #**************************************************************************
  #*  Show search results
  #**************************************************************************
  #if ($tab == "opac") {
  #  require_once("../shared/header_opac.php");
  #} else {
  #  require_once("../shared/header.php");
  #}
  #require_once("../classes/Localize.php");
  #$loc = new Localize(OBIB_LOCALE,"shared");
$get_local = $_GET['local'];
if ( $get_local != "")
$loc = new Localize($get_local,"shared");
else
{
$loc = new Localize(OBIB_LOCALE,"shared");
//$get_local = "en";
}
    $allBiblioArray = array();
  # Display no results message if no results returned from search.
  if ($biblioQ->getRowCount() == 0) {
    $biblioQ->close();
    //echo $loc->getText("biblioSearchNoResults");
    //echo ;
    //$allBiblioArray = array();
    //echo json_encode($allBiblioArray); 
    //#require_once("../shared/footer.php");
    exit();
  }
?>
<?php 
  //echo $loc->getText("biblioSearchResultTxt",array("items"=>$biblioQ->getRowCount()));
  //if ($biblioQ->getRowCount() > 1) {
   // echo $loc->getText("biblioSearch".$sortBy);
 // }
?>
  <?php
    $priorBibid = -2;
    $biblioArray = array();
    $biblioCopyArray = array();
    while ($biblio = $biblioQ->fetchRow()) {
      if ($biblio->getBibid() == $priorBibid) {
        if ($biblio->getBarcodeNmbr() != "") {
          #************************************
          #* append copy line to copies array
          #************************************
          //echo H($biblioQ->getCurrentRowNmbr());
          $biblioCopyArray[$loc->getText("biblioSearchCopyBCode")]=H($biblio->getBarcodeNmbr());
          if ($lookup == 'Y') {
                //<a href="javascript:returnLookup('barcodesearch','barcodeNmbr','<?php echo H(addslashes($biblio->getBarcodeNmbr()));">echo $loc->getText("biblioSearchOutIn"); echo H(addslashes($biblio->getBarcodeNmbr()));echo $loc->getText("biblioSearchHold"); 
              } 
            $biblioCopyArray[$loc->getText("biblioSearchCopyStatus")]=H($biblioStatusDm[$biblio->getStatusCd()]);
            array_push($biblioArray["copies"],$biblioCopyArray);
        }
      }
      else {
        if (!empty($biblioArray))
          array_push($allBiblioArray,$biblioArray);
        $priorBibid = $biblio->getBibid();
        $biblioArray = array();
        $biblioCopyArray = array();

      //$biblioArray["seq"]=H($biblioQ->getCurrentRowNmbr());
      $biblioArray["url"]="../shared/biblio_view.php?bibid=".HURL($biblio->getBibid())."&amp;tab=".HURL($tab)."&amp;local=".H($get_local);
      $biblioArray[$loc->getText("biblioSearchTitle")]=$biblio->getTitle();
      $biblioArray[$loc->getText("biblioSearchAuthor")]=$biblio->getAuthor();
      $biblioArray[$loc->getText("biblioSearchMaterial")]=$materialTypeDm[$biblio->getMaterialCd()];
      $biblioArray[$loc->getText("biblioSearchCollection")]=$collectionDm[$biblio->getCollectionCd()];
      //$biblioArray[$loc->getText("biblioSearchCall")]=H($biblio->getCallNmbr1()." ".$biblio->getCallNmbr2()." ".$biblio->getCallNmbr3());
      $biblioArray[$loc->getText("biblioSearchCall")]=H($biblio->getCallNmbr1()." ".$biblio->getCallNmbr2()." ".$biblio->getCallNmbr3());
      //if ($biblio->getBarcodeNmbr() != "") {
        $biblioArray[$loc->getText("biblioSearchCopyBCode")]=H($biblio->getBarcodeNmbr());

        //$biblioCopyArray[$loc->getText("biblioSearchCopyBCode")]=H($biblio->getBarcodeNmbr());
          if ($lookup == 'Y') { 
            //echo H(addslashes($biblio->getBarcodeNmbr()));
            //echo $loc->getText("biblioSearchOutIn"); echo H(addslashes($biblio->getBarcodeNmbr()));
            //echo $loc->getText("biblioSearchHold"); 
          } 
          //$biblioCopyArray[$loc->getText("biblioSearchCopyStatus")]=H($biblioStatusDm[$biblio->getStatusCd()]);
          $biblioArray[$loc->getText("biblioSearchCopyStatus")]=H($biblioStatusDm[$biblio->getStatusCd()]);          
          //$biblioArray["copies"]=array();
          //array_push($biblioArray["copies"],$biblioCopyArray);
      //}
    }
}
    $biblioQ->close();
//print_r($allBiblioArray);
echo json_encode($allBiblioArray); 
//printResultPages($loc, $currentPageNmbr, $biblioQ->getPageCount(), $sortBy); ?>
