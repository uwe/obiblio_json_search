$.getScript('config.js',function() {
/*read configs from config.js*/

/* Data expected looks similar to the below structure (might vary depending on language)*/
// var data = {[
//     { 
//       "title"         : item.title, //[0]
//       "link"          : item.link, //[1]
//       "author"        : item.author, //[2]
//       "location"      : item.location, //[3]
//       "collection"    : item.collection, //[4]
//       "call_number"   : item.callNumber, //[5]
//       "copy_barcode"  : item.copyBarcode,
//       "status"        : item.status
//     }
//   ]};

$('#pharsesearch').on( "submit", function( event ) {
    event.preventDefault();
    var formData =  $(this).serializeArray();
    var keyword = formData[0]['value'];
    var searchText = formData[1]['value'];
    $(config.libraries.urls).each(function(i,item){
        var divID = "tabs-"+item.id;
        if(!$('ul#tabsul').is(':has(li#elem-'+item.id+')')){
            $("ul#tabsul").append("<li id=\"elem-"+item.id+"\"><a href='#tabs-"+item.id+"'>"+item.name+"</a></li>");
            $("div#tabs").append("<div id='tabs-"+item.id+"'><img src=\"images/ajax.gif\" /></div>");
        }   
        $.ajax({/* proxy url can be defined in the configs */
             url:config.proxyurl+"?url="+item.url+"&full_headers=1", 
                type: "POST", 
                data:{
                 tab: "opac", 
                 sortBy : "default", 
                 lookup: "Y" , 
                 searchType : keyword, /* we do keyword search only (TODO?)*/
                 searchText: searchText
                },
                datatype: "json",
                // crossDomain : true
            })
            .done(function(data){
                // console.log(data.contents);
                var result = data.contents;
                if(typeof(result) === 'string')
                {
                  $('#'+divID).html(result);
                }
                else
                {
                  var fieldsName={}, firstItem = result[0];

                  var i=0;
                  for (var key in firstItem)
                  {
                    if(i === 1){
                        fieldsName.title = key;
                    }                                        
                    if(i === 2){
                        fieldsName.author = key;
                    }
                    if(i === 3){
                        fieldsName.material = key;
                    }                     
                    if(i === 4){
                        fieldsName.collection = key;
                    }                     
                    if(i === 5){
                        fieldsName.call_number = key;
                    }
                    if(i === 6){
                        fieldsName.copies = key;
                    }
                    i++;
                  }


                  var entries ='<ul id="tag-result-'+divID+'" style="list-style:none;margin:0;padding:0">';
                  $(result).each(function(i,field){                                                    
                      entries +='<li><table width="100%" style="border:1px solid black;border-collapse:none">'+
                                  '<tbody><tr>'+
                                      '<td class="noborder" width="1%" valign="top"><b>'+fieldsName.title+':</b></td>'+
                                      '<td class="noborder" colspan="3"><a href="'+field.url+'" target="_blank">'+field[fieldsName.title]+'</a></td>'+
                                    '</tr><tr>'+
                                      '<td class="noborder" valign="top"><b>'+fieldsName.author+':</b></td>'+
                                      '<td class="noborder" colspan="3">'+field[fieldsName.author]+'</td>'+
                                    '</tr><tr>'+
                                      '<td class="noborder" valign="top"><font class="small"><b>'+fieldsName.material+':</b></font></td>'+
                                      '<td class="noborder" colspan="3"><font class="small">'+field[fieldsName.material]+'</font></td>'+
                                    '</tr><tr>'+
                                      '<td class="noborder" valign="top"><font class="small"><b>'+fieldsName.collection+':</b></font></td>'+
                                      '<td class="noborder" colspan="3"><font class="small">'+field[fieldsName.collection]+'</font></td>'+
                                    '</tr><tr>'+
                                      '<td class="noborder" valign="top" nowrap="yes"><font class="small"><b>'+fieldsName.call_number+':</b></font></td>'+
                                      '<td class="noborder" colspan="3"><font class="small">'+field[fieldsName.call_number]+'</font></td>'+
                                    '</tr><tr>';
                                      // '<td class="noborder" valign="top" nowrap="yes"><font class="small"><b>'+fieldsName.copies+':</b></font></td>'+
                                      // '<td class="noborder" colspan="3"><font class="small"><table>';
                                      // $(field.copies).each(function(n,copyItem){
                                      //     entries+='<tr><td>Status :'+copyItem.Status+'</td></tr>';
                                      // });
                      entries +='</tbody></table></li>';

                  });
                  entries +='</ul>';

                  $('#'+divID).html(entries);
                  $('#tag-result-'+divID).quickPagination({pageSize:"3"});             
                }
            });
    });

    $( "#tabs" ).tabs({
        activate: function(event, ui) {   
                var newPanel = ui.newPanel;
                             
                window.location.hash = $(newPanel).attr('id');
            }
    }).addClass( "ui-tabs-vertical ui-helper-clearfix" );
    
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-right" );
    return false;
});

  	// var encoded_url =  encodeURIComponent("http://192.168.99.143/obiblio1/shared/biblio_search_json.php");
   //  var libQuery = $.ajax({
   //  					url:"./proxy.php?url="+encoded_url+"&full_headers=1", 
   //                      type: "POST", 
   //                      data:{
   //                      	tab: "opac", 
   //                      	sortBy : "default", 
   //                      	lookup: "Y" , 
   //                      	searchType : "keyword",
   //                      	searchText: ""
   //                      },
   //                      datatype: "json",
   //                      crossDomain : true
   //                  })
   //                  .done(function(data){
   //                      var result = data.responseText;
   //                      data =  JSON.parse(result);

   //                      var entries ="";
   //                      $.each(data.contents, function(i,item){
   //                          entries+="<p><a href='"+data.contents[i].url+"' target='_blank'>"+data.contents[i].Title+"</a></p>" ;
   //                      });
                        
   //                      console.log("this ran ... wallahi");
   //                      $('#tabs-4').html(entries);
   //                  });
});
